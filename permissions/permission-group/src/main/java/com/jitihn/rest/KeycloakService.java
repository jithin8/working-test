package com.jitihn.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.jitihn.dtos.GroupDTO;
import com.jitihn.dtos.UserDTO;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/keycloak")
@RegisterRestClient
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface KeycloakService {

    @GET
    @Path("/group")
    List<GroupDTO> getGroups();

    @POST
    @Path("/group")
    Response createGroup(GroupDTO groupDTO);

    @GET
    @Path("/user")
    List<UserDTO> getUser();

    @GET
    @Path("/user/{uid}")
    Response deleteUser(@PathParam("uid") String uid);

    @POST
    @Path("/user")
    Response createUser(UserDTO groupDTO);

    // @GET
    // @Path("/realm/{realmId}")
    // Response getRealm(@PathParam String realmId);
}