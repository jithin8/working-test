package com.jitihn.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.validation.ConstraintViolation;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {
    private static Logger logger = LoggerFactory.getLogger(Utils.class);

    private static JSONObject objectToJsonObject(Object object) {
        String jsonString = objectToJson(object);
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (Exception err) {
            logger.info("Error", err.toString());
        }
        return jsonObject;
    }

    public static String generateUniqueId() {
        return UUID.randomUUID().toString();
    }

    private static String objectToJson(Object object) {
        Jsonb jsonb = JsonbBuilder.create();
        return jsonb.toJson(object);
    }

    // private Object jsonToObject(String json, Object object) {
    // Jsonb jsonb = JsonbBuilder.create();
    // return jsonb.fromJson(json, object.getClass());
    // }

    private static Object createResponseMessage(Map<String, Object> jsonObj) {
        for (Map.Entry<String, Object> entry : jsonObj.entrySet()) {
            jsonObj.put(entry.getKey(), entry.getValue());
        }
        return objectToJsonObject(jsonObj);
    }

    // public static String validationResult(Set<? extends ConstraintViolation<?>>
    // violations) {
    // return violations.stream().map(cv ->
    // cv.getMessage()).collect(Collectors.joining(", "));
    // }

    public static Response JsonResponse(Status status, String message, boolean isSuccess, Map<String, Object> jsonObj) {
        Object msgObj = null;
        if (jsonObj == null) {
            jsonObj = new HashMap<String, Object>();
        }
        jsonObj.put("success", isSuccess);
        jsonObj.put("message", message);
        msgObj = createResponseMessage(jsonObj);
        return Response.status(status).entity(msgObj.toString()).build();
    }

    public static String getStringFromDate(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return date.format(formatter);
    }

    // dd-MM-yyyy
    public static LocalDate getDateFromString(String date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }
}