package com.jitihn.resource;

import java.util.HashMap;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dtos.GroupDTO;
import com.jitihn.rest.KeycloakService;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/api/group")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GroupResource {

    @Inject
    @RestClient
    KeycloakService keycloakService;

    @POST
    public Response create(GroupDTO dto) {
        Response res = keycloakService.createGroup(dto);
        if (res.getStatus() == 202) {
            return Utils.JsonResponse(Status.OK, "group created", true, null);
        }
        if (res.getStatus() == 200) {
            return Utils.JsonResponse(Status.OK, "group already exist", false, null);
        }
        return Utils.JsonResponse(Status.BAD_REQUEST, "something wrong", false, null);

    }

    @GET
    public Response get() {
        HashMap<String, Object> obj = new HashMap<>();
        obj.put("data", keycloakService.getGroups());
        return Utils.JsonResponse(Status.ACCEPTED, "created", true, obj);
    }
}