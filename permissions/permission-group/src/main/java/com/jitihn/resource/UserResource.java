package com.jitihn.resource;

import java.util.HashMap;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.jitihn.dtos.UserDTO;
import com.jitihn.rest.KeycloakService;
import com.jitihn.utils.Utils;

import org.eclipse.microprofile.rest.client.inject.RestClient;

@Path("/api/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UserResource {

    @Inject
    @RestClient
    KeycloakService keycloakService;

    @POST
    public Response create(UserDTO dto) {
        Response res = keycloakService.createUser(dto);
        System.out.println(res.getStatus());
        if (res.getStatus() == 202) {
            return Utils.JsonResponse(Status.OK, "user created", true, null);
        }
        if (res.getStatus() == 200) {
            return Utils.JsonResponse(Status.OK, "user already exist", false, null);
        }
        return Utils.JsonResponse(Status.BAD_REQUEST, "something wrong", false, null);

    }

    @GET
    public Response get() {
        HashMap<String, Object> obj = new HashMap<>();
        obj.put("data", keycloakService.getUser());
        return Utils.JsonResponse(Status.ACCEPTED, "listed", true, obj);
    }

    @DELETE
    @Path("/{uid}")
    public Response delete(@PathParam("uid") String uid) {
        System.out.println(uid);
        HashMap<String, Object> obj = new HashMap<>();
        // obj.put("data", uid);
        obj.put("data", keycloakService.deleteUser(uid));
        return Utils.JsonResponse(Status.ACCEPTED, "deleted", true, obj);
    }
}