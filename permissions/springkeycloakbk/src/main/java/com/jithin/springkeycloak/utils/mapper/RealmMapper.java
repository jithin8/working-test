package com.jithin.springkeycloak.utils.mapper;

import java.util.List;

import com.jithin.springkeycloak.dtos.RealmDTO;
import com.jithin.springkeycloak.dtos.RoleDTO;
import com.jithin.springkeycloak.dtos.UserDTO;

import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;

public class RealmMapper {

    public static List<RealmDTO> mapRealmIntoDTOs(List<RealmRepresentation> entities) {
        return entities.stream().map(RealmMapper::mapRealmIntoDTO).collect(toList());
    }

    public static RealmDTO mapRealmIntoDTOs(RealmRepresentation entity) {
        return mapRealmIntoDTO(entity);
    }

    public static List<RoleDTO> mapRoleIntoDTOs(List<RoleRepresentation> entities) {
        return entities.stream().map(RealmMapper::mapRoleIntoDTO).collect(toList());
    }

    public static List<UserDTO> mapUserIntoDTOs(List<UserRepresentation> entities) {
        return entities.stream().map(RealmMapper::mapUserIntoDTOs).collect(toList());
    }

    public static RoleDTO mapRoleIntoDTOs(RoleRepresentation entity) {
        return mapRoleIntoDTO(entity);
    }

    public static UserDTO mapUserIntoDTOs(UserRepresentation entity) {
        return mapUsetIntoDTO(entity);
    }

    private static UserDTO mapUsetIntoDTO(UserRepresentation entity) {
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setEmail(entity.getEmail());
        dto.setRealmRoles(entity.getRealmRoles());
        dto.setEnabled(entity.isEnabled());
        return dto;
    }

    public static UserRepresentation mapDtoIntoUser(UserDTO dto) {
        UserRepresentation user = new UserRepresentation();
        user.setUsername(dto.getUsername());
        user.setCredentials(dto.getCredentialRepresentation());
        user.setEmail(dto.getEmail());
        user.setRealmRoles(dto.getRealmRoles());
        user.setEnabled(dto.getEnabled());
        return user;
    }

    static RealmDTO mapRealmIntoDTO(RealmRepresentation entity) {
        RealmDTO dto = new RealmDTO();
        dto.setDisplayName(entity.getDisplayName());
        dto.setEnabled(entity.isEnabled());
        dto.setId(entity.getId());
        dto.setRealm(entity.getRealm());
        return dto;
    }

    static RoleDTO mapRoleIntoDTO(RoleRepresentation entity) {
        RoleDTO dto = new RoleDTO();
        dto.setRoleId(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setRealmId(entity.getContainerId());
        dto.setRoleName(entity.getName());
        return dto;
    }

}