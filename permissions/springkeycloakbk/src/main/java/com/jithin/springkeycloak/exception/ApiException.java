package com.jithin.springkeycloak.exception;

import org.springframework.http.HttpStatus;

public class ApiException {
    private final Boolean status;
    private final String message;
    private final HttpStatus httpStatus;

    public Boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ApiException(Boolean status, String message, HttpStatus httpStatus) {
        this.status = status;
        this.message = message;
        this.httpStatus = httpStatus;
    }

}