package com.jithin.springkeycloak.controller.keycloak;

import java.util.List;
import com.jithin.springkeycloak.dtos.RealmDTO;
import com.jithin.springkeycloak.dtos.RoleDTO;
import com.jithin.springkeycloak.exception.ApiRequestException;
import com.jithin.springkeycloak.service.KeycloakService;

import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/keycloak/")
public class RoleController {

    private KeycloakService keycloakService;

    @Autowired
    public RoleController(KeycloakService keycloakService) {
        this.keycloakService = keycloakService;
    }

    @GetMapping("{realmId}/role")
    public List<RoleDTO> getRole(@PathVariable String realmId) {
        return keycloakService.getRoles(realmId);
    }

    @GetMapping("{realmId}/roleraw")
    public List<RoleRepresentation> getRoleRaw(@PathVariable String realmId) {
        return keycloakService.getRolesRaw(realmId);
    }

    @GetMapping("{realmId}/role/{roleId}")
    public ResponseEntity<RoleDTO> getRoleById(@PathVariable String realmId, @PathVariable String roleId) {
        RoleDTO rdto = keycloakService.getRole(realmId, roleId);
        return rdto != null ? ResponseEntity.status(HttpStatus.OK).body(rdto)
                : ResponseEntity.status(HttpStatus.NO_CONTENT).body(new RoleDTO());
    }

    @PostMapping("{realmId}/role")
    @ResponseBody
    RoleDTO createRole(@PathVariable String realmId, @RequestBody RoleDTO roleDTO) {
        roleDTO.setRealmId(realmId);
        keycloakService.createRole(roleDTO);
        return keycloakService.getRole(roleDTO.getRealmId(), roleDTO.getRoleId());
    }

    @DeleteMapping("{realmId}/role/{roleId}")
    public ResponseEntity deleteByRoleId(@PathVariable String realmId, @PathVariable String roleId) {
        keycloakService.deleteRoleById(realmId, roleId);
        return ResponseEntity.status(HttpStatus.OK).body(true);
    }

}