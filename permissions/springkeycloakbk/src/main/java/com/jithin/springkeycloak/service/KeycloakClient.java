package com.jithin.springkeycloak.service;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;

public class KeycloakClient {

    static Keycloak keycloak = null;
    final static String serverUrl = "http://localhost:8181/auth";
    final static String realm = "admin-realm";
    final static String clientId = "admin-client";
    final static String clientSecret = "585c8c1c-b439-46a0-a359-fb2151c9657c";

    public KeycloakClient() {
    }

    public static Keycloak getInstance() {
        if (keycloak == null) {
            // keycloak = KeycloakBuilder.builder() //
            // .serverUrl(serverUrl) //
            // .realm(realm) //
            // .grantType(OAuth2Constants.CLIENT_CREDENTIALS) //
            // .clientId(clientId) //
            // .clientSecret(clientSecret) //
            // .build();

            keycloak = KeycloakBuilder.builder() //
                    .serverUrl(serverUrl) //
                    .realm("master") //
                    .grantType(OAuth2Constants.PASSWORD) //
                    .clientId("admin-cli") //
                    .username("admin") //
                    .password("admin") //
                    .build();
        }
        return keycloak;
    }
}