package com.jithin.springkeycloak.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import com.jithin.springkeycloak.dtos.RealmDTO;
import com.jithin.springkeycloak.dtos.RoleDTO;
import com.jithin.springkeycloak.dtos.UserDTO;
import com.jithin.springkeycloak.utils.mapper.RealmMapper;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

@Service
public class KeycloakService {

    // realms
    public List<RealmDTO> getRealms() {
        Keycloak kc = KeycloakClient.getInstance();
        List<RealmRepresentation> repr = kc.realms().findAll();

        return RealmMapper.mapRealmIntoDTOs(repr);
    }

    public List<RealmRepresentation> getRawRealms() {
        Keycloak kc = KeycloakClient.getInstance();
        List<RealmRepresentation> repr = kc.realms().findAll();

        return repr;
    }

    public void createRealm(String realmName) {
        RealmRepresentation rep = new RealmRepresentation();
        rep.setRealm(realmName);
        rep.setEnabled(true);
        Keycloak kc = KeycloakClient.getInstance();
        kc.realms().create(rep);
    }

    public RealmDTO getRealmById(String realmId) {
        Keycloak kc = KeycloakClient.getInstance();
        RealmRepresentation rep = kc.realm(realmId).toRepresentation();
        return RealmMapper.mapRealmIntoDTOs(rep);
    }

    public void deleteById(String realmId) {
        Keycloak kc = KeycloakClient.getInstance();
        kc.realms().realm(realmId).remove();
    }

    // role

    public List<RoleDTO> getRoles(String realmId) {
        Keycloak kc = KeycloakClient.getInstance();
        List<RoleRepresentation> repr = kc.realms().realm(realmId).roles().list();
        return RealmMapper.mapRoleIntoDTOs(repr);
    }

    public void createRole(RoleDTO roleDTO) {
        Keycloak kc = KeycloakClient.getInstance();
        RoleRepresentation rep = new RoleRepresentation(roleDTO.getRoleId(), roleDTO.getDescription(), false);
        // rep.setId(roleDTO.getRoleId());
        System.out.println(roleDTO);
        System.out.println(rep);
        kc.realms().realm(roleDTO.getRealmId()).roles().create(rep);
        System.out.println("success");
        // System.out.println(kc.realms().realm(roleDTO.getRealmId()).toRepresentation().getRealm());
    }

    public RoleDTO getRole(String realmId, String roleId) {
        Keycloak kc = KeycloakClient.getInstance();
        RoleRepresentation rep = null;
        try {
            rep = kc.realms().realm(realmId).roles().get(roleId).toRepresentation();
            return RealmMapper.mapRoleIntoDTOs(rep);
        } catch (Exception exception) {
            System.out.println("Catch exception");
            return null;
        }
    }

    public void deleteRole(String realmId, String roleId) {
        Keycloak kc = KeycloakClient.getInstance();
        kc.realms().realm(realmId).roles().get(roleId).remove();
    }

    public List<RoleRepresentation> getRolesRaw(String realmId) {
        Keycloak kc = KeycloakClient.getInstance();
        List<RoleRepresentation> repr = kc.realms().realm(realmId).roles().list();
        return repr;
    }

    public void deleteRoleById(String realmId, String roleId) {
        Keycloak kc = KeycloakClient.getInstance();
        kc.realms().realm(realmId).roles().deleteRole(roleId);
    }

    public UserDTO getUser(String realmId, String userId) {
        Keycloak kc = KeycloakClient.getInstance();
        UserRepresentation rep = kc.realms().realm(realmId).users().get(userId).toRepresentation();
        return RealmMapper.mapUserIntoDTOs(rep);
    }

    public void deleteUserById(String realmId, String userId) {
        Keycloak kc = KeycloakClient.getInstance();
        kc.realms().realm(realmId).users().delete(userId);
    }

    public boolean createUser(String realmId, UserDTO dto) {

        Keycloak kc = KeycloakClient.getInstance();

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation = RealmMapper.mapDtoIntoUser(dto);

        RealmResource realmResource = kc.realm(realmId);
        UsersResource userRessource = realmResource.users();

        // Create user (requires manage-users role)
        Response response = userRessource.create(userRepresentation);
        if (response.getStatus() == 201) {
            System.out.println("Repsonse: " + response.getStatusInfo());
            System.out.println(response.getLocation());
            String userId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");

            System.out.printf("User created with userId: %s%n", userId);

            List<String> roles = dto.getRealmRoles();
            List<RoleRepresentation> roleList = getRoleRepFromString(realmResource, roles);

            // Assign realm role tester to user
            userRessource.get(userId).roles().realmLevel() //
                    .add(roleList);

            addRoleToUser(userRessource, userId, roleList);

            return true;
        }
        return false;
    }

    private List<RoleRepresentation> getRoleRepFromString(RealmResource realmResource, List<String> roles) {
        List<RoleRepresentation> roleRep = new ArrayList<>();
        System.out.println("now check");
        for (String role : roles) {
            System.out.println(role);
            RoleResource roleRes = realmResource.roles().get(role);
            try {
                System.out.println(roleRes.toRepresentation().getName());
                roleRep.add(roleRes.toRepresentation());
            } catch (Exception e) {
                System.out.println("exception ");
            }
        }
        return roleRep;
    }

    private void addRoleToUser(UsersResource usersResource, String userId, List<RoleRepresentation> roleList) {
        usersResource.get(userId).roles().realmLevel() //
                .add(roleList);
    }

    private void addRoleToUser(String realmId, String userId, List<RoleRepresentation> roleList) {
        Keycloak kc = KeycloakClient.getInstance();
        RealmResource realmResource = kc.realm(realmId);
        UsersResource userRessource = realmResource.users();
        userRessource.get(userId).roles().realmLevel() //
                .add(roleList);

        System.out.println("added role");
    }

    public List<UserDTO> getUsers(String realmId) {
        Keycloak kc = KeycloakClient.getInstance();
        List<UserRepresentation> repr = kc.realms().realm(realmId).users().list();
        return RealmMapper.mapUserIntoDTOs(repr);
    }

}