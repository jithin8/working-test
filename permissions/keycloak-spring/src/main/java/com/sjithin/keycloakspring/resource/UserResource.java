package com.sjithin.keycloakspring.resource;

import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.PathParam;

import com.sjithin.keycloakspring.dao.UserDAO;
import com.sjithin.keycloakspring.dto.UserDTO;

import org.keycloak.representations.idm.CredentialRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class UserResource {
    private UserDAO userDAO;

    @Autowired
    public UserResource(UserDAO dao) {
        this.userDAO = dao;
    }

    @PostMapping(value = "/api/keycloak/user")
    @ResponseBody
    public ResponseEntity<UserDTO> create(@RequestBody UserDTO dto) {
        dto.setEnabled(true);
        CredentialRepresentation repr = new CredentialRepresentation();
        repr.setType("password");
        repr.setTemporary(false);
        repr.setValue(dto.getPassword());
        dto.setCredentials(Arrays.asList(repr));
        if (userDAO.create(dto)) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping(value = "/api/keycloak/user")
    public List<UserDTO> get() {
        return userDAO.get();
    }

    @GetMapping("/api/keycloak/user/{userId}")
    UserDTO get(@PathVariable String userId) {
        return userDAO.get(userId);
    }

    @DeleteMapping("/api/keycloak/user/{uid}")
    public Boolean delete(@PathVariable("uid") String uid) {
        System.out.println("user id " + uid);
        // userDAO.delete(uid);
        return true;
    }

}