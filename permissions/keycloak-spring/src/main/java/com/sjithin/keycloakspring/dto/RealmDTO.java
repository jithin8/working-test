package com.sjithin.keycloakspring.dto;

public class RealmDTO {

    private String id;
    private String realm;
    private String displayName;
    private Boolean enabled;

    public RealmDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "RealmModel [displayName=" + displayName + ", enabled=" + enabled + ", id=" + id + ", realm=" + realm
                + "]";
    }

}