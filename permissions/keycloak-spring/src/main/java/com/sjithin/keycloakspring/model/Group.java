package com.sjithin.keycloakspring.model;

public class Group {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}