package com.sjithin.keycloakspring.resource;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import com.sjithin.keycloakspring.dao.GroupDAO;
import com.sjithin.keycloakspring.dto.GroupDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class GroupResource {
    private GroupDAO groupDAO;

    @Autowired
    public GroupResource(GroupDAO dao) {
        this.groupDAO = dao;
    }

    @PostMapping(value = "/api/keycloak/group")
    public ResponseEntity<GroupDTO> create(@RequestBody GroupDTO dto) {
        try {
            dto = groupDAO.create(dto);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(dto);
        } catch (Exception e) {
            System.out.println("execption " + e);
            return ResponseEntity.status(HttpStatus.OK).build();
        }
    }

    @GetMapping(value = "/api/keycloak/group")
    public List<GroupDTO> get() {
        return groupDAO.get();
    }

}