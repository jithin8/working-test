package com.sjithin.keycloakspring.dao;

import java.util.List;

import javax.ws.rs.core.Response;

import com.sjithin.keycloakspring.dto.GroupDTO;
import com.sjithin.keycloakspring.keycloak.KeycloakService;
import com.sjithin.keycloakspring.utils.Mapper;
import com.sjithin.keycloakspring.utils.Utils;

import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class GroupDAO {

    @Autowired
    KeycloakService keycloakService;

    @Value("${keycloak.realm}")
    private String realmName;

    public List<GroupDTO> get() {
        GroupsResource groupsResource = keycloakService.getRealm().groups();
        List<GroupRepresentation> reprs = groupsResource.groups();
        return Mapper.mapGroupIntoDTOs(reprs);
    }

    public GroupDTO create(GroupDTO dto) {
        GroupRepresentation group = new GroupRepresentation();
        group.setName(dto.getName());
        group = createGroup(keycloakService.getRealm(), group);
        return Mapper.mapGroupIntoDTO(group);
    }

    private GroupRepresentation createGroup(RealmResource realm, GroupRepresentation group) {
        try (Response response = realm.groups().add(group)) {
            String groupId = Utils.getCreatedId(response);
            // Set ID to the original rep
            group.setId(groupId);
            return group;
        }
    }
}