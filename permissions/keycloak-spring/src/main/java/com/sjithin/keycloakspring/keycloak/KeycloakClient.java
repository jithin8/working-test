package com.sjithin.keycloakspring.keycloak;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.stereotype.Service;

@Service
public class KeycloakClient {
    private static Keycloak keycloak = null;
    private static String serverUrl = "http://localhost:8181/auth";

    public static Keycloak getInstance() {
        if (keycloak == null) {
            keycloak = KeycloakBuilder.builder() //
                    .serverUrl(serverUrl) //
                    .realm("master") //
                    .grantType(OAuth2Constants.PASSWORD) //
                    .clientId("admin-cli") //
                    .username("admin") //
                    .password("admin") //
                    .build();
        }
        return keycloak;
    }
}