package com.sjithin.keycloakspring.dao;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;
import com.sjithin.keycloakspring.dto.UserDTO;
import com.sjithin.keycloakspring.keycloak.KeycloakService;
import com.sjithin.keycloakspring.utils.Mapper;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class UserDAO {

    @Autowired
    KeycloakService keycloakService;

    @Value("${keycloak.realm}")
    private String realmName;

    public List<UserDTO> get() {
        List<UserRepresentation> repr = keycloakService.getRealm().users().list();
        return Mapper.mapUserIntoDTOs(repr);
    }

    public boolean create(UserDTO dto) {

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation = Mapper.mapDtoIntoUser(dto);

        UsersResource userResource = keycloakService.getRealm().users();

        // Create user (requires manage-users role)
        Response response = userResource.create(userRepresentation);
        if (response.getStatus() == 201) {
            System.out.println("Repsonse: " + response.getStatusInfo());
            System.out.println(response.getLocation());
            String userId = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");

            System.out.printf("User created with userId: %s%n", userId);

            List<String> roles = dto.getRealmRoles();
            List<RoleRepresentation> roleList = getRoleRepFromString(roles);

            List<String> groups = dto.getGroups();
            List<GroupRepresentation> groupList = getGroupRepFromString(groups);
            System.out.println("jithin group list");
            System.out.println(groupList);
            // Assign realm role tester to user
            // userRessource.get(userId).roles().realmLevel() //
            // .add(roleList);

            addRoleToUser(userResource, userId, roleList);

            for (GroupRepresentation gr : groupList) {
                addGroupToUser(userResource, userId, gr);
            }
            return true;
        }
        return false;
    }

    private List<RoleRepresentation> getRoleRepFromString(List<String> roles) {
        List<RoleRepresentation> roleRep = new ArrayList<>();
        System.out.println("now check");
        if (roles != null && roles.size() > 0) {

            for (String role : roles) {
                System.out.println(role);
                RoleResource roleRes = keycloakService.getRealm().roles().get(role);
                try {
                    System.out.println(roleRes.toRepresentation().getName());
                    roleRep.add(roleRes.toRepresentation());
                } catch (Exception e) {
                    System.out.println("exception ");
                }
            }
        }
        return roleRep;
    }

    private List<GroupRepresentation> getGroupRepFromString(List<String> groups) {
        List<GroupRepresentation> groupRep = new ArrayList<>();
        if (groups != null && groups.size() > 0) {

            for (String group : groups) {
                GroupResource gr = getGroupResourceFromGroupName(group);
                try {
                    groupRep.add(gr.toRepresentation());
                } catch (Exception e) {
                    System.out.println("exception ");
                }
            }
        }
        return groupRep;
    }

    private GroupResource getGroupResourceFromGroupName(String name) {
        List<GroupRepresentation> reps = keycloakService.getRealm().groups().groups();

        for (GroupRepresentation representation : reps) {
            if (name.equalsIgnoreCase(representation.getName())) {
                return keycloakService.getRealm().groups().group(representation.getId());
            }
        }
        return null;
    }

    private void addRoleToUser(UsersResource usersResource, String userId, List<RoleRepresentation> roleList) {
        usersResource.get(userId).roles().realmLevel() //
                .add(roleList);
    }

    private void addGroupToUser(UsersResource usersResource, String userId, GroupRepresentation groupRepresentation) {
        usersResource.get(userId).joinGroup(groupRepresentation.getId());
    }

    // private void addRoleToUser(String realmId, String userId,
    // List<RoleRepresentation> roleList) {
    // UsersResource userRessource = keycloakService.getRealm().users();
    // userRessource.get(userId).roles().realmLevel() //
    // .add(roleList);

    // System.out.println("added role");
    // }

    public UserDTO get(String userId) {
        UserRepresentation rep = keycloakService.getRealm().users().get(userId).toRepresentation();
        return Mapper.mapUserIntoDTO(rep);
    }

    public void delete(String userId) {
        keycloakService.getRealm().users().delete(userId);
    }
}