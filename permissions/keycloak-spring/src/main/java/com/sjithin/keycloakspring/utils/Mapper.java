package com.sjithin.keycloakspring.utils;

import java.util.List;

import com.sjithin.keycloakspring.dto.GroupDTO;
import com.sjithin.keycloakspring.dto.RealmDTO;
import com.sjithin.keycloakspring.dto.UserDTO;

import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import static java.util.stream.Collectors.toList;

public class Mapper {
    public static List<RealmDTO> mapRealmIntoDTOs(List<RealmRepresentation> entities) {
        return entities.stream().map(Mapper::mapRealmIntoDTO).collect(toList());
    }

    public static List<GroupDTO> mapGroupIntoDTOs(List<GroupRepresentation> entities) {
        return entities.stream().map(Mapper::mapGroupIntoDTO).collect(toList());
    }

    public static List<UserDTO> mapUserIntoDTOs(List<UserRepresentation> entities) {
        return entities.stream().map(Mapper::mapUserIntoDTO).collect(toList());
    }

    public static GroupDTO mapGroupIntoDTO(GroupRepresentation entity) {
        GroupDTO dto = new GroupDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setPath(entity.getPath());
        dto.setAttributes(entity.getAttributes());
        dto.setRealmRoles(entity.getRealmRoles());
        dto.setClientRoles(entity.getClientRoles());
        dto.setSubGroups(entity.getSubGroups());
        dto.setAccess(entity.getAccess());
        return dto;
    }

    private static RealmDTO mapRealmIntoDTO(RealmRepresentation entity) {
        RealmDTO dto = new RealmDTO();
        dto.setDisplayName(entity.getDisplayName());
        dto.setEnabled(entity.isEnabled());
        dto.setId(entity.getId());
        dto.setRealm(entity.getRealm());
        return dto;
    }

    public static UserDTO mapUserIntoDTO(UserRepresentation entity) {
        UserDTO dto = new UserDTO();
        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setEmail(entity.getEmail());
        dto.setRealmRoles(entity.getRealmRoles());
        dto.setEnabled(entity.isEnabled());
        dto.setGroups(entity.getGroups());
        return dto;
    }

    public static UserRepresentation mapDtoIntoUser(UserDTO dto) {
        UserRepresentation user = new UserRepresentation();
        user.setUsername(dto.getUsername());
        user.setCredentials(dto.getCredentials());
        user.setEmail(dto.getEmail());
        user.setRealmRoles(dto.getRealmRoles());
        user.setEnabled(dto.getEnabled());
        user.setGroups(dto.getGroups());
        return user;
    }

}