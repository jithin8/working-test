package com.sjithin.keycloakspring.keycloak;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class KeycloakService {
    @Value("${keycloak.realm}")
    private String realmName;

    public RealmResource getRealm() {
        Keycloak kc = KeycloakClient.getInstance();
        RealmResource realm = kc.realms().realm(realmName);
        return realm;
    }

}