package com.jitihn.resource;

import java.security.Permission;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.security.auth.AuthPermission;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.quarkus.security.ForbiddenException;
import io.quarkus.security.identity.SecurityIdentity;

@Path("/api/product")
@Produces(MediaType.APPLICATION_JSON)
// @Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {

    // @GET
    // public Response get() {
    // return Response.ok("product").build();
    // }

    @Inject
    SecurityIdentity identity;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CompletionStage<List<Permission>> get() {
        System.out.println("jtihin");
        // System.out.println(identity.getPrincipal().toString());
        System.out.println(identity.getAttributes());

        // System.out.println("" + identity.getAttribute("permissions"));
        return identity.checkPermission(new AuthPermission("product-resource")).thenCompose(granted -> {
            if (granted) {
                System.out.println("jithin");
                System.out.println(identity.getPrincipal().getName());
                // System.out.println(identity.getPrincipal().implies("arg0"));
                return CompletableFuture.completedFuture(identity.getAttribute("permissions"));
            }
            throw new ForbiddenException();
        });
    }
}