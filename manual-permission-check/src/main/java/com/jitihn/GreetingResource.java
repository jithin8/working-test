package com.jitihn;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;
import javax.security.auth.AuthPermission;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.keycloak.representations.idm.authorization.Permission;

import io.quarkus.security.identity.SecurityIdentity;

@Path("/hello")
public class GreetingResource {

    // @GET
    // @Produces(MediaType.TEXT_PLAIN)
    // public String hello() {
    // return "hello";
    // }

    @Inject
    SecurityIdentity identity;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CompletionStage<List<Permission>> get() {
        System.out.println("jtihin");
        // System.out.println(identity.getPrincipal().toString());
        System.out.println(identity.getAttributes());
        // System.out.println("" + identity.getAttribute("permissions"));
        return identity.checkPermission(new AuthPermission("Permission Resource")).thenCompose(granted -> {
            if (granted) {
                return CompletableFuture.completedFuture(identity.getAttribute("permissions"));
            }
            throw new ForbiddenException();
        });
    }
}